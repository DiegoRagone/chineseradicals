"use client";
import { RadicalContext } from "@/context/RadicalContext";
import Link from "next/link";
import { useContext, useMemo, useRef, useState } from "react";

const Quiz = () => {
  const { _filteredRadicals, $filteredRadicals } = useContext(RadicalContext);
  const answer = useRef(null);

  const selectAnswer = () => {
    if (!answer?.current) {
      return alert("Seleziona una risposta.");
    }
    if (answer?.current == selectedRadical.selection.id) {
      alert("Risposta Corretta!");
      $filteredRadicals((prev) => [...prev]);
      answer.current = null;
    } else {
      alert("Risposta Errata, riprova.");
    }
  };

  const getRandomIndexes = () => {
    const array = [];
    const randomIndex = Math.floor(Math.random() * _filteredRadicals.length);
    array.push(randomIndex);

    for (let index = 0; index < 3; index++) {
      getRandomIndex(array, 0);
    }

    return array;
  };

  const getRandomIndex = (array, counter) => {
    const newRandomIndex = Math.floor(Math.random() * _filteredRadicals.length);
    if (array.includes(newRandomIndex) && counter <= 3) {
      counter++;
      console.log(counter);
      getRandomIndex(array, counter);
    } else {
      array.push(newRandomIndex);
    }
  };

  let selectedRadical = useMemo(() => {
    const radi = [..._filteredRadicals];
    const array = getRandomIndexes();
    const selection = radi[array[0]];
    const selection2 = radi[array[1]];
    const selection3 = radi[array[2]];
    const selection4 = radi[array[3]];
    return { selection, selection4, selection2, selection3 };
  }, [_filteredRadicals]);

  const responses = [];

  for (const key in selectedRadical) {
    let loopElement = selectedRadical[key];

    responses.push(
      <div
        className="form-check"
        onClick={() => (answer.current = loopElement.id)}
        key={`answer_${loopElement?.italian}`}
      >
        <input className="form-check-input" type="radio" name="flexRadioDefault" id={loopElement?.italian} />
        <label className="form-check-label " htmlFor={loopElement?.italian}>
          {loopElement?.italian}
        </label>
      </div>
    );
  }

  function shuffle(array) {
    let currentIndex = array.length;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {
      // Pick a remaining element...
      let randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
    }
  }

  shuffle(responses);

  const questionVariants = selectedRadical?.selection?.variants?.map((e, i) => (
    <h1 key={`variant_${e}_${i}`} style={{ fontSize: "50px", fontFamily: "Noto Serif SC" }} className="text-center">
      {e}
    </h1>
  ));

  return (
    <div className="container position-relative">
      <div className="position-absolute" style={{ top: 30, right: 20 }}>
        {questionVariants}
      </div>
      <div className="pt-2 ">
        <Link href={"/"} className="me-5">
          Indietro
        </Link>
        <h5 className="text-center">N° {selectedRadical?.selection?.id}</h5>
      </div>
      <h1 style={{ fontSize: "200px", fontFamily: "Noto Serif SC" }} className="text-center radical">
        {selectedRadical?.selection?.radical}
      </h1>
      <div>{responses}</div>
      <div className="d-flex justify-content-between mt-4">
        <h2 onClick={() => $filteredRadicals((prev) => [...prev])}>Reset</h2>
        <h2 onClick={() => selectAnswer()}>Conferma</h2>
      </div>
    </div>
  );
};

export default Quiz;
