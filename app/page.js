"use client";
import Main from "@/components/Main";
import { RadicalProvider } from "@/context/RadicalContext";
import App from "next/app";

export default function Home() {
  return <Main />;
}
