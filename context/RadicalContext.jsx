"use client";
import { radicals } from "@/radicalsObj";
import { createContext, useState } from "react";

export const RadicalContext = createContext();

export const RadicalProvider = ({ children }) => {
  const [_radicals, $radicals] = useState(radicals);
  const [_selectedStrokes, $selectedStrokes] = useState([]);
  const [_filteredRadicals, $filteredRadicals] = useState([]);

  const obj = {
    _radicals,
    $radicals,
    _selectedStrokes,
    $selectedStrokes,
    $filteredRadicals,
    _filteredRadicals,
  };

  return <RadicalContext.Provider value={obj}>{children}</RadicalContext.Provider>;
};
