"use client";

import { RadicalContext } from "@/context/RadicalContext";
import { radicals } from "@/radicalsObj";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";

const Main = () => {
  const { $selectedStrokes, _selectedStrokes, _radicals, $filteredRadicals, _filteredRadicals } =
    useContext(RadicalContext);

  const strokesUnique = [...new Set(_radicals.map((item) => item.strokeCount))];

  const toggleStroke = (e) => {
    const array = [..._selectedStrokes];
    const index = array.indexOf(e);
    if (index > -1) {
      array.splice(index, 1);
    } else {
      array.push(e);
    }

    $selectedStrokes(array);
  };

  useEffect(() => {
    const filtered = _radicals?.filter((rad) => _selectedStrokes?.includes(rad?.strokeCount));
    $filteredRadicals(filtered);
  }, [_selectedStrokes]);

  const strokesOptions = strokesUnique?.map((e, i) => (
    <div key={`strokeCountOption_${e}`} className="form-check form-switch" onChange={() => toggleStroke(e)}>
      <input className="form-check-input mx-2" type="checkbox" id="flexSwitchCheckDefault" />
      <label className="form-check-label" htmlFor="flexSwitchCheckDefault">
        {e}
      </label>
    </div>
  ));

  return (
    <div className="container">
      <div className=" ">
        <h2>Seleziona il numero di tratti</h2>
        {strokesOptions}
      </div>
      <div className="text-end mt-3">
        <Link href={"/quiz"}>
          <h2>Vai al Quiz</h2>
        </Link>
      </div>
    </div>
  );
};

export default Main;
